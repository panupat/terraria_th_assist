# -*- coding: utf-8 -*-
import io
import json
import sys
from os import environ
from os.path import dirname, join, expanduser, normpath, isfile, abspath, basename
from PySide2.QtGui import QIcon, QFont, Qt
from PySide2.QtWidgets import (QApplication, QWidget, QHBoxLayout, QLabel, QPushButton, QVBoxLayout,
                               QFileDialog, QComboBox, QMessageBox, QTreeWidget, QTreeWidgetItem,
                               QTextEdit, QDialog, QLineEdit)
import qdarkstyle


def resourcePath(relative):
    return join(
        environ.get(
            "_MEIPASS2",
            abspath(".")
        ),
        relative
    )

_currentPath = dirname(__file__)
_defaultDataPath = normpath(join(_currentPath, 'data'))
_icon = resourcePath('img/icon.png')
_filePrefix = 'Terraria.Localization.Content'


class TerrariaThAssist(QWidget):
    def __init__(self, *args, **kwargs):
        super(TerrariaThAssist, self).__init__(*args, **kwargs)
        self.setWindowTitle('Terraria TH assistant')
        self.setWindowIcon(QIcon(_icon))
        self.setFont(QFont('Tahoma', 10))
        self.setMinimumWidth(1000)
        self.setMinimumHeight(800)
        self.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
        self.currentItem = None
        self.dstFile = None

        labelWidth = 120
        mainLayout = QVBoxLayout()

        row0 = QHBoxLayout()
        l0 = QLabel(r'เลือกโฟลเดอร์ :')
        l0.setFixedWidth(labelWidth)
        l0.setAlignment(Qt.AlignRight)
        row0.addWidget(l0)
        self.baseDirectory = QLabel(_defaultDataPath)
        self.baseDirectory.setMinimumWidth(300)
        row0.addWidget(self.baseDirectory)
        browseBtn = QPushButton(r'เลือกโฟลเดอร์')
        row0.addWidget(browseBtn)
        row0.addStretch()

        row1 = QHBoxLayout()
        lsrc = QLabel(r'ต้นฉบับ:ปลายทาง')
        self.srcLang = QLineEdit('en-US')
        self.srcLang.setFixedWidth(60)
        lcolon = QLabel(r':')
        self.dstLang = QLineEdit('ru-RU')
        self.dstLang.setFixedWidth(60)
        row1.addWidget(lsrc)
        row1.addWidget(self.srcLang)
        row1.addWidget(lcolon)
        row1.addWidget(self.dstLang)
        l1 = QLabel(r'เลือกไฟล์ :')
        # l1.setFixedWidth(labelWidth)
        l1.setAlignment(Qt.AlignRight)
        row1.addWidget(l1)
        self.fileChooser = QComboBox()
        row1.addWidget(self.fileChooser)
        fileLoadBtn = QPushButton(r'โหลด')
        row1.addWidget(fileLoadBtn)
        row1.addStretch()

        self.fileChooser.addItems(['Base', 'Game', 'Items', 'Legacy', 'NPCs', 'Projectiles', 'Town'])

        self.resultTree = QTreeWidget()
        self.resultTree.setHeaderLabels(['', 'ต้นฉบับ', 'ปลายทาง'])
        self.resultTree.setStyleSheet("""
            QTreeWidget {font-size: 11pt; font-face: Tahoma; alternate-background-color: #242929;}
            QTreeWidget::item { border-bottom: 1px solid #444444; padding: 5px;}""")
        self.resultTree.setAlternatingRowColors(True)
        self.resultTree.setColumnWidth(0, 200)
        self.resultTree.setColumnWidth(1, 300)
        self.resultTree.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        row2 = QHBoxLayout()
        saveAsBtn = QPushButton('  Save as  ')
        row2.addStretch()
        row2.addWidget(saveAsBtn)

        mainLayout.addLayout(row0)
        mainLayout.addLayout(row1)
        mainLayout.addWidget(self.resultTree)
        mainLayout.addLayout(row2)
        self.setLayout(mainLayout)

        browseBtn.clicked.connect(self.onBrowseClicked)
        fileLoadBtn.clicked.connect(self.onFileLoadClicked)
        self.resultTree.itemDoubleClicked.connect(self.onItemDoubleClicked)
        saveAsBtn.clicked.connect(self.saveAs)

        self.editorDialog = QDialog()
        self.editorDialog.setWindowTitle('แก้ไข')
        self.editorSrcText = QTextEdit()
        self.editorSrcText.setReadOnly(True)
        self.editorDstText = QTextEdit()
        editorBtn = QPushButton('OK')
        editorLayout = QVBoxLayout()
        editorLayout.addWidget(self.editorSrcText)
        editorLayout.addWidget(self.editorDstText)
        editorLayout.addWidget(editorBtn)
        self.editorDialog.setLayout(editorLayout)
        self.editorDialog.setWindowIcon(QIcon(_icon))
        self.editorDialog.setFont(QFont('Tahoma', 11))
        self.editorDialog.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
        self.editorDialog.setMinimumWidth(300)
        editorBtn.clicked.connect(self.onEditorOkClicked)

    def onBrowseClicked(self):
        self.browseFolder()

    def onFileLoadClicked(self):
        self.loadFile()

    def onItemDoubleClicked(self, item):
        self.editItemText(item)

    def onEditorOkClicked(self):
        self.applyEdit()

    def browseFolder(self):
        current = self.baseDirectory.text()
        if not current:
            current = _defaultDataPath
        dst = QFileDialog.getExistingDirectory(None, 'Select a folder:', current)
        if dst:
            self.baseDirectory.setText(dst)
        else:
            self.baseDirectory.setText(current)

    def loadFile(self):
        if not self.baseDirectory.text():
            QMessageBox.warning(self, r'ผิดพลาด', 'ยังไม่ได้เลือก Folder')
            return
        whichFile = self.fileChooser.currentText()
        if whichFile == 'Base':
            whichFile = ''
        else:
            whichFile += '.'
        srcFile = normpath(
            join(self.baseDirectory.text(), '{}.{}.{}json'.format(_filePrefix, self.srcLang.text(), whichFile)))
        dstFile = normpath(
            join(self.baseDirectory.text(), '{}.{}.{}json'.format(_filePrefix, self.dstLang.text(), whichFile)))
        for f in srcFile, dstFile:
            if not isfile(f):
                QMessageBox.warning(self, r'ผิดพลาด', 'ไม่พบไฟล์ %s' % f)
                return
        self.dstFile = dstFile
        try:
            with io.open(srcFile, 'r', encoding='utf8') as s:
                srcData = json.load(s)
        except:
            QMessageBox.critical(self, r'ไฟล์ JSON มีจุดผิดพลาด',
                                 '%s มีจุดผิดพลาด กรุณาเช็คไฟล์' % basename(srcFile))
            return
        try:
            with io.open(dstFile, 'r', encoding='utf8') as d:
                dstData = json.load(d)
        except:
            QMessageBox.critical(self, r'ไฟล์ JSON มีจุดผิดพลาด',
                                 '%s มีจุดผิดพลาด กรุณาเช็คไฟล์' % basename(dstFile))
            return
        self.resultTree.clear()
        self.showResult(srcData, dstData)

    def showResult(self, srcData, dstData):
        for key0 in srcData:
            parent = QTreeWidgetItem([key0])
            for key1 in srcData[key0]:
                srcText = srcData[key0][key1]
                if key1 in dstData[key0]:
                    dstText = dstData[key0][key1]
                else:
                    dstText = ''
                child = QTreeWidgetItem(parent, [key1, srcText, dstText])
            self.resultTree.addTopLevelItem(parent)

    def editItemText(self, item):
        if not item.columnCount() > 1:
            self.currentItem = None
            return
        self.currentItem = item
        self.editorDialog.show()
        self.editorDialog.raise_()
        self.editorSrcText.setText(item.text(1))
        self.editorDstText.setText(item.text(2))

    def applyEdit(self):
        self.editorDialog.close()
        self.currentItem.setText(2, self.editorDstText.toPlainText())

    def saveAs(self):
        if not self.dstFile:
            return
        filename = QFileDialog.getSaveFileName(
            self, 'Save as', self.dstFile)
        self.doSave(filename[0])

    def doSave(self, filename):
        result = self.readEditorToDict()
        with io.open(filename, 'w', encoding='utf8') as o:
            json.dump(result, o, ensure_ascii=False, indent=4)

    def readEditorToDict(self):
        result = {}
        root = self.resultTree.invisibleRootItem()
        for i in range(root.childCount()):
            parent = root.child(i)
            result[parent.text(0)] = {}
            for j in range(parent.childCount()):
                child = parent.child(j)
                result[parent.text(0)][child.text(0)] = child.text(2)
        return result


if __name__ == '__main__':
    app = QApplication(sys.argv)
    test = TerrariaThAssist()
    test.show()
    sys.exit(app.exec_())
