# Terraria Thai Translator Assistant Tool

A little script I created to help Thai translators translating Terraria. ru-RU version was used as base so it was set as default.

#### Usage
Point to directory with both source(reference) and destination file. Pick which file to translate (Base/Items/NPCs/ etc) and click the button. Double-click a line to open up editor. Save As button on the bottom right corner.

#### Windows exe
[Can be downloaded here.](C:\Users\joey\Downloads\drive-download-20190719T033813Z-001) 

#### Source Code
You're looking at it ( 〃 ω〃)
